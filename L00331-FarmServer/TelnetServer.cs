﻿
// Thanks are owed to examples found on the web :-)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace FarmServer
{
    public class TelnetServer
    {
        private Socket serverSocket;
        private IPAddress ip;
        private int port = 23;
        private readonly int dataSize;
        private byte[] data;
        private bool doEcho = false;
        private Dictionary<Socket, TelnetClient> clients;

        public delegate void ConnectionEventHandler(TelnetClient c);
        public event ConnectionEventHandler ClientConnected;
        public event ConnectionEventHandler ClientDisconnected;
        public delegate void MessageReceivedEventHandler(TelnetClient c, string message);
        public event MessageReceivedEventHandler MessageReceived;

        public TelnetServer(IPAddress ip, int port, int dataSize = 1024)
        {
            this.ip = ip;
            this.port = port;
            this.dataSize = dataSize;
            this.data = new byte[dataSize];

            this.clients = new Dictionary<Socket, TelnetClient>();

            this.serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Start()
        {
            serverSocket.Bind(new IPEndPoint(ip, port));
            serverSocket.Listen(0);
            serverSocket.BeginAccept(new AsyncCallback(HandleIncomingConnection), serverSocket);
        }

        public void Stop()
        {
            serverSocket.Close();
        }

        public void SendMessageToClient(TelnetClient c, string message)
        {
            Socket clientSocket = GetSocketByClient(c);
            SendMessageToSocket(clientSocket, message);

            // Note: it would be better to handle this at a higher level but this is quick and easy
            string verbose = ">>> (" + clientSocket.RemoteEndPoint.ToString() + ") Telnet: " + message;
            verbose = verbose.Replace("\r", "");
            verbose = verbose.Replace("\n", "");
            Console.WriteLine(verbose);
            Log.Add(Configuration.CommLogFile, verbose);
        }

        private void SendMessageToSocket(Socket s, string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            SendBytesToSocket(s, data);
        }

        private void SendBytesToSocket(Socket s, byte[] data)
        {
            s.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendData), s);
        }

        private TelnetClient GetClientBySocket(Socket clientSocket)
        {
            if (!clients.TryGetValue(clientSocket, out TelnetClient c))
                c = null;

            return c;
        }

        private Socket GetSocketByClient(TelnetClient client)
        {
            Socket s;

            s = clients.FirstOrDefault(x => x.Value.GetClientID() == client.GetClientID()).Key;

            return s;
        }

        public void KickClient(TelnetClient client)
        {
            CloseSocket(GetSocketByClient(client));
            ClientDisconnected(client);
        }

        private void CloseSocket(Socket clientSocket)
        {
            clientSocket.Close();
            clients.Remove(clientSocket);
        }

        public List<string> GetClientList()
        {
            List<string> clientList = new List<string>();

            foreach(var client in clients)
            {
                clientList.Add(client.ToString());
            }
            return clientList;
        }

        private void HandleIncomingConnection(IAsyncResult result)
        {
            try
            {
                Socket oldSocket = (Socket)result.AsyncState;

                Socket newSocket = oldSocket.EndAccept(result);

                uint clientID = (uint)clients.Count + 1;
                TelnetClient client = new TelnetClient(clientID, (IPEndPoint)newSocket.RemoteEndPoint);
                clients.Add(newSocket, client);

                SendBytesToSocket(
                    newSocket,
                    new byte[] {
                            0xff, 0xfd, 0x01,   // Do Echo
                            0xff, 0xfd, 0x21,   // Do Remote Flow Control
                            0xff, 0xfb, 0x01,   // Will Echo
                            0xff, 0xfb, 0x03    // Will Supress Go Ahead
                    }
                );

                client.ResetReceivedData();

                ClientConnected(client);

                serverSocket.BeginAccept(new AsyncCallback(HandleIncomingConnection), serverSocket);
            }

            catch { }
        }

        private void SendData(IAsyncResult result)
        {
            try
            {
                Socket clientSocket = (Socket)result.AsyncState;

                clientSocket.EndSend(result);

                clientSocket.BeginReceive(data, 0, dataSize, SocketFlags.None, new AsyncCallback(ReceiveData), clientSocket);
            }

            catch { }
        }

        private void ReceiveData(IAsyncResult result)
        {
            try
            {
                Socket clientSocket = (Socket)result.AsyncState;
                TelnetClient client = GetClientBySocket(clientSocket);

                int bytesReceived = clientSocket.EndReceive(result);

                if (bytesReceived == 0)
                {
                    CloseSocket(clientSocket);
                    serverSocket.BeginAccept(new AsyncCallback(HandleIncomingConnection), serverSocket);
                }

                else if (data[0] < 0xF0)
                {
                    string receivedData = client.GetReceivedData();

                    // 0x2E = '.', 0x0D = carriage return, 0x0A = new line
                    if ((data[0] == 0x2E && data[1] == 0x0D && receivedData.Length == 0) ||
                        (data[0] == 0x0D && data[1] == 0x0A))
                    {
                        //sendMessageToSocket(clientSocket, "\u001B[1J\u001B[H");
                        if (doEcho) SendBytesToSocket(clientSocket, new byte[] { data[0] });
                        string receivedVata = client.GetReceivedData();
                        // Note: it would be better to handle this at a higher level but this is quick and easy

                        string verbose = "<<< (" +  clientSocket.RemoteEndPoint.ToString() + ") Telnet: " + receivedData;
                        Console.WriteLine(verbose);
                        Log.Add(Configuration.CommLogFile, verbose);

                        MessageReceived(client, receivedData);
                        client.ResetReceivedData();

                    }

                    else
                    {
                        // 0x08 => backspace character
                        if (data[0] == 0x08)
                        {
                            if (receivedData.Length > 0)
                            {
                                client.RemoveLastCharacterReceived();
                                SendBytesToSocket(clientSocket, new byte[] { 0x08, 0x20, 0x08 });
                            }

                            else
                                clientSocket.BeginReceive(data, 0, dataSize, SocketFlags.None, new AsyncCallback(ReceiveData), clientSocket);
                        }

                        // 0x7F => delete character
                        else if (data[0] == 0x7F)
                            clientSocket.BeginReceive(data, 0, dataSize, SocketFlags.None, new AsyncCallback(ReceiveData), clientSocket);

                        else
                        {
                            client.AppendReceivedData(Encoding.ASCII.GetString(data, 0, bytesReceived));

                            // Echo back the received character
                            if (doEcho) SendBytesToSocket(clientSocket, new byte[] { data[0] });

                            clientSocket.BeginReceive(data, 0, dataSize, SocketFlags.None, new AsyncCallback(ReceiveData), clientSocket);
                        }
                    }
                }

                else
                    clientSocket.BeginReceive(data, 0, dataSize, SocketFlags.None, new AsyncCallback(ReceiveData), clientSocket);
            }

            catch { }
        }
    }
}
