﻿
using System.IO;

namespace FarmServer
{
    static class Configuration
    {
        private const string ConfigurationFile = "FarmServerConfiguration.txt";

        public const int MaxRoomNumber = 4;
        public const int FirstWallOffset = 100;
        public const int NorthWallOffset = 100;
        public const int RoomOffset = 200;
        public const int NbSouthWalls = 11;
        public const int NbNorthWalls = 11;

        public static int TelnetPort = -1;
        public static int DamatexWatchdogTimeoutSeconds = 0;
        public static int DamatexWatchdogTimeoutPwmDutyCycle = 10;
        public static int LightingRefreshIntervalSeconds = -1;
        public static int LightingRefreshDelayBetweenControllersMilliseconds = -1;
        public static int LightingInitialRefreshIntervalSeconds = -1;
        public static string LightingRs485ComPort = "";
        public static int LightingRs485RxTimeoutMilliseconds = -1;
        public static string ServerVersion = "";
        public static int ServerId = -1;
        public static int FirstRoom = 1;
        public static int LastRoom = 1;
        public static int MaxAgeOfStatusFileMinutes = -1;
        public static string ErrorLogFile = "";
        public static string CommLogFile = "";

        private static bool Validate()
        {
            if (DamatexWatchdogTimeoutSeconds < 0) return false;
            if (DamatexWatchdogTimeoutPwmDutyCycle < 0) return false;
            if (LightingInitialRefreshIntervalSeconds <= 0) return false;
            if (LightingRefreshDelayBetweenControllersMilliseconds <= 0) return false;
            if (LightingRefreshIntervalSeconds <= 0) return false;
            if (!LightingRs485ComPort.ToUpper().StartsWith("COM")) return false;
            if (LightingRs485RxTimeoutMilliseconds <= 0) return false;
            if (FirstRoom < 1 || FirstRoom > MaxRoomNumber) return false;
            if (LastRoom < 1 || LastRoom > MaxRoomNumber) return false;
            if (FirstRoom > LastRoom) return false;
            if (MaxAgeOfStatusFileMinutes < 0) return false;
            if (ServerId < 0) return false;
            if (string.IsNullOrEmpty(ServerVersion)) return false;
            if (TelnetPort <= 0 || TelnetPort > 65535) return false;
            if (CommLogFile == "") return false;
            if (ErrorLogFile == "") return false;

            return true;
        }

        public static bool Load()
        {
            if (!File.Exists(ConfigurationFile))
            {
                Log.Add(Configuration.ErrorLogFile, "Configuration file is missing!");
                return false;
            }

            foreach (string line in System.IO.File.ReadLines(ConfigurationFile))
            {
                if (string.IsNullOrEmpty(line)) continue;
                if (line.StartsWith("*")) continue;

                string[] split = line.Split('=');
                if (split.Length != 2)
                {
                    Log.Add(Configuration.ErrorLogFile, "Configuration file is invalid!");
                    return false;
                }

                switch (split[0])
                {
                    case "DamatexWatchdogTimeoutSeconds":
                        int.TryParse(split[1], out DamatexWatchdogTimeoutSeconds);
                        break;
                    case "DamatexWatchdogTimeoutPwmDutyCycle":
                        int.TryParse(split[1], out DamatexWatchdogTimeoutPwmDutyCycle);
                        break;
                    case "FirstRoom":
                        int.TryParse(split[1], out FirstRoom);
                        break;
                    case "LastRoom":
                        int.TryParse(split[1], out LastRoom);
                        break;
                    case "LightingInitialRefreshIntervalSeconds":
                        int.TryParse(split[1], out LightingInitialRefreshIntervalSeconds);
                        break;
                    case "LightingRefreshDelayBetweenControllersMilliseconds":
                        int.TryParse(split[1], out LightingRefreshDelayBetweenControllersMilliseconds);
                        break;
                    case "LightingRefreshIntervalSeconds":
                        int.TryParse(split[1], out LightingRefreshIntervalSeconds);
                        break;
                    case "LightingRs485ComPort":
                        LightingRs485ComPort = split[1];
                        break;
                    case "LightingRs485RxTimeoutMilliseconds":
                        int.TryParse(split[1], out LightingRs485RxTimeoutMilliseconds);
                        break;
                    case "MaxAgeOfStatusFileMinutes":
                        int.TryParse(split[1], out MaxAgeOfStatusFileMinutes);
                        break;
                    case "ServerId":
                        int.TryParse(split[1], out ServerId);
                        break;
                    case "ServerVersion":
                        ServerVersion = split[1];
                        break;
                    case "TelnetPort":
                        int.TryParse(split[1], out TelnetPort);
                        break;
                    case "ErrorLogFile":
                        ErrorLogFile = split[1];
                        break;
                    case "CommLogFile":
                        CommLogFile = split[1];
                        break;
                }
            }

            if (!Validate())
            {
                Log.Add(Configuration.ErrorLogFile, "Configuration file contains invalid or missing parameters!");
                return false;
            }

            return true;
        }

    }

}

