﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using System.Threading;

namespace FarmServer
{
    public static class LightingService
    {
        public static bool RefreshEnabled = true;

        #region Controller Status

        public static class ControllerStatuses
        {
            private readonly static string statFile = "FarmServerControllerStatus.txt";

            private static List<Status> statusList = new List<Status>();
            private static Mutex mutex = new Mutex();

            public class Status
            {
                public int WallId;
                public bool Alive;
                public int Ch1;
                public int Ch2;
                public int Ch1D;

                public Status(int wallId, bool alive, int ch1, int ch2, int ch1D)
                {
                    WallId = wallId;
                    Alive = alive;
                    Ch1 = ch1;
                    Ch2 = ch2;
                    Ch1D = ch1D;
                }
            };

            private static string FormatWallController(bool alive, int ch, int ch1D)
            {
                if (!alive) return "-";
                if (ch1D >= 0) return "*";
                if (ch >= 100) return "A";
                string s = (ch / 10).ToString();
                return s.Substring(0, 1);
            }

            private static string FormatWall(int firstWallId, int lastWallId, int channel)
            {
                // Careful! caller must protect statusList with mutex

                string wallFormat = "";

                for (int wallId = firstWallId; wallId <= lastWallId; wallId++)
                {
                    int i = 0;
                    for (i = 0; i < statusList.Count; i++)
                    {
                        if (statusList[i].WallId == wallId)
                        {
                            if (statusList[i].Alive)
                            {
                                wallFormat += FormatWallController(statusList[i].Alive, channel == 1 ? statusList[i].Ch1 : statusList[i].Ch2, statusList[i].Ch1D);
                            }
                            else
                            {
                                wallFormat += "X";
                            }
                            break;
                        }
                    }
                    if (i == statusList.Count)
                    {
                        wallFormat += ".";
                    }
                }

                return wallFormat;                
            }

            public static string SimpleFormat()
            {
                mutex.WaitOne();

                bool globalStatus = true;
                for (int controller = 0; controller < statusList.Count; controller++)
                {
                    if (!statusList[controller].Alive)
                    {
                        globalStatus = false;
                        break;
                    }
                }

                mutex.ReleaseMutex();

                return globalStatus ? "OK" : "FAIL";
            }

            public static string NormalFormat(string room)
            {
                int.TryParse(room, out int roomNumber);

                string formattedStats = "";

                mutex.WaitOne();

                int errCode = 0;
                for (int wall = 0; wall < statusList.Count; wall++)
                {
                    if (!statusList[wall].Alive)
                    {
                        errCode = 1;
                        break;
                    }
                }
                formattedStats = errCode.ToString("") + ",";

                for (int wall = 0; wall < statusList.Count; wall++)
                {
                    if (roomNumber != 0)
                    {
                        int wallId = statusList[wall].WallId;
                        int r = (wallId - Configuration.FirstWallOffset) / Configuration.RoomOffset + 1;
                        if (r != roomNumber) continue;
                    }

                    if (!formattedStats.EndsWith(",")) formattedStats += ",";
                    formattedStats += statusList[wall].WallId.ToString() + ",";
                    formattedStats += statusList[wall].Ch1.ToString() + ",";
                    formattedStats += statusList[wall].Ch1D.ToString() + ",";
                    formattedStats += statusList[wall].Ch2.ToString();
                }

                mutex.ReleaseMutex();

                return formattedStats;
            }

            public static string HumanReadableFormat()
            {
                mutex.WaitOne();

                string formattedStats = "";

                for (int room=Configuration.FirstRoom; room <= Configuration.LastRoom; room++)
                {
                    int roomOffset = (room - 1) * Configuration.RoomOffset;
                    formattedStats += "Room " + room.ToString() + "\r\n";
                    formattedStats += "12345678901\r\n";
                    formattedStats += FormatWall(roomOffset + Configuration.FirstWallOffset + Configuration.NorthWallOffset + 1, roomOffset + Configuration.FirstWallOffset + Configuration.NorthWallOffset + Configuration.NbNorthWalls, 1) + " North CH1\r\n";
                    formattedStats += FormatWall(roomOffset + Configuration.FirstWallOffset + 1, roomOffset + Configuration.FirstWallOffset + Configuration.NbSouthWalls, 1) + " South CH1\r\n";
                    formattedStats += FormatWall(roomOffset + Configuration.FirstWallOffset + Configuration.NorthWallOffset + 1, roomOffset + Configuration.FirstWallOffset + Configuration.NorthWallOffset + Configuration.NbNorthWalls, 2) + " North CH2\r\n";
                    formattedStats += FormatWall(roomOffset + Configuration.FirstWallOffset + 1, roomOffset + Configuration.FirstWallOffset + Configuration.NbSouthWalls, 2) + " South CH2\r\n";
                }

                mutex.ReleaseMutex();

                return formattedStats;
            }

            public static bool Load()
            {
                bool success = true;

                mutex.WaitOne();

                try
                {
                    statusList.Clear();

                    foreach (string line in File.ReadLines(statFile))
                    {
                        CommandParser.ParsedCommand pc = CommandParser.Parse(line);
                        if (!pc.Success)
                        {
                            Log.Add(Configuration.ErrorLogFile, "Status file is invalid!");
                            success = false;
                            break;
                        }

                        int ch1 = 0;
                        int ch2 = 0;
                        for (int i = 0; i < pc.ParameterNames.Count; i++)
                        {
                            if (pc.ParameterNames[i].ToUpper() == "CH1") int.TryParse(pc.ParameterValues[i], out ch1);
                            if (pc.ParameterNames[i].ToUpper() == "CH2") int.TryParse(pc.ParameterValues[i], out ch2);
                        }

                        statusList.Add(new Status(pc.wallId, false, ch1, ch2, -1));
                    }

                    if (statusList.Count == 0)
                    {
                        Log.Add(Configuration.ErrorLogFile, "Status file is empty!");
                        success = false;
                    }
                    else
                    {
                        // If file is too old, just reset all cotrollers to 0

                        DateTime fileTime = File.GetLastWriteTime(statFile);
                        if (DateTime.Now.Subtract(fileTime).TotalMinutes >= Configuration.MaxAgeOfStatusFileMinutes)
                        {
                            for (int c = 0; c < statusList.Count; c++)
                            {
                                statusList[c].Ch1 = 0;
                                statusList[c].Ch2 = 0;
                            }

                        }

                    }

                }
                catch
                {
                    Log.Add(Configuration.ErrorLogFile, "Status file is missing!");
                    success = false;
                }

                // If status file cannot be read (missing, invalid, empty) create default

                if (success == false)
                {
                    for (int room = Configuration.FirstRoom; room <= Configuration.LastRoom; room++)
                    {
                        int roomOffset = (room - 1) * Configuration.RoomOffset;

                        for (int w = Configuration.FirstWallOffset + 1; w <= Configuration.FirstWallOffset + Configuration.NbSouthWalls; w++)
                        {
                            statusList.Add(new Status(roomOffset + w, false, 0, 0, -1));
                        }
                        for (int w = Configuration.FirstWallOffset + Configuration.NorthWallOffset + 1; w <= Configuration.FirstWallOffset + Configuration.NorthWallOffset + Configuration.NbNorthWalls; w++)
                        {
                            statusList.Add(new Status(roomOffset + w, false, 0, 0, -1));
                        }
                    }
                    success = true;
                }

                mutex.ReleaseMutex();

                return success;
            }

            private static bool Save()
            {
                // Careful! caller must protect statusList with mutex

                bool success = true;

                try
                {
                    using (StreamWriter sw = new StreamWriter(statFile))
                    {
                        for (int controller = 0; controller < statusList.Count; controller++)
                        {
                            sw.WriteLine("/" + statusList[controller].WallId.ToString("000") + "/" +
                                        "CH1=" + statusList[controller].Ch1.ToString() + "," +
                                        "CH2=" + statusList[controller].Ch2.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    Log.Add(Configuration.ErrorLogFile, "Cannot save the statuses to file " + statFile + ": " + ex.Message);
                }

                return success;
            }

            public static int Count()
            {
                return statusList.Count;
            }

            public static Status GetFromIndex(int index)
            {
                if (index < 0 || index >= statusList.Count) return null;

                mutex.WaitOne();
                ControllerStatuses.Status status = statusList[index];
                mutex.ReleaseMutex();

                return status;
            }

            public static bool DimmingEngaged(int controllerId)
            {
                bool dimming = false;

                mutex.WaitOne();

                for (int c=0; c<statusList.Count; c++)
                {
                    if (statusList[c].WallId == controllerId)
                    {
                        dimming = statusList[c].Ch1D >= 0;
                        break;
                    }
                }

                mutex.ReleaseMutex();

                return dimming;
            }

            public static void UpdateAlive(int controllerId, bool alive)
            {
                mutex.WaitOne();

                for (int controller = 0; controller < statusList.Count; controller++)
                {
                    if (statusList[controller].WallId == controllerId)
                    {
                        statusList[controller].Alive = alive;
                        break;
                    }
                }

                mutex.ReleaseMutex();
            }

            public static int WhichRoom(int wallId)
            {
                return (wallId - Configuration.FirstWallOffset) / Configuration.RoomOffset;
            }

            public static bool IsRoomBroadcastAddress(int wallId)
            {
                return wallId % 100 == 0;
            }

            public static int UpdateChannels(int wallId, int ch1, int ch1D, int ch2)
            {
                mutex.WaitOne();

                int index = -1;
                for (int c = 0; c < statusList.Count; c++)
                {
                    if (statusList[c].WallId == wallId || 
                        (IsRoomBroadcastAddress(wallId) && WhichRoom(statusList[c].WallId) == WhichRoom(wallId)))
                    {
                        // Use cases:       CH1 CH2 CH1D
                        //  CH1=20          20, -1, -1
                        //  CH2=55          -1, 55, -1
                        //  CH1=20,CH2=55   20, 55, -1
                        //  CH1D=15         -1, -1, 15
                        //  CH1D=OFF        -1, -1, -2
                        if (ch1 != -1) statusList[c].Ch1 = ch1;
                        if (ch2 != -1) statusList[c].Ch2 = ch2;
                        if (ch1D != -1) statusList[c].Ch1D = ch1D;
                        index = c;
                    }
                }

                Save();

                mutex.ReleaseMutex();

                return index;
            }

        }

        #endregion

        #region Lighting Service

        public static bool Initialize()
        {
            if (!ControllerStatuses.Load())
            {
                Log.Add(Configuration.ErrorLogFile, "Cannot load statuses!");
                return false;
            }

            System.Timers.Timer timer = new System.Timers.Timer(Configuration.LightingInitialRefreshIntervalSeconds * 1000);
            timer.Elapsed += new ElapsedEventHandler(RefreshTimer);
            timer.Enabled = true;

            return true;
        }

        public static bool DimmingEngaged(CommandParser.ParsedCommand pc)
        {
            foreach (string name in pc.ParameterNames)
            {
                if (name == "CH1D") return false;
            }
            return ControllerStatuses.DimmingEngaged(pc.wallId);
        }

        private static void UpdateController(int controllerId, int ch1, int ch1D, int ch2)
        {
            int index = ControllerStatuses.UpdateChannels(controllerId, ch1, ch1D, ch2);

            if (index == -1) return;

            ControllerStatuses.Status status = ControllerStatuses.GetFromIndex(index);

            string command = "/" + controllerId.ToString("000") + "/";
            if (status.Ch1D < 0)
            {
                command += "CH1=" + status.Ch1.ToString() + ",CH2=" + status.Ch2.ToString();
            }
            else
            {
                command += "CH1=" + status.Ch1D.ToString() + ",CH2=0";
            }

            bool success = ControllerLink.Relay(
                                Configuration.LightingRs485ComPort, Configuration.LightingRs485RxTimeoutMilliseconds, command);

            ControllerStatuses.UpdateAlive(controllerId, success);
        }

        public static void HandleCommand(CommandParser.ParsedCommand pc)
        {
            // For now only CH1, CH1D and CH2 are supported

            int ch1 = -1;
            int ch1D = -1;
            int ch2 = -1;

            // -1 means parameter not specified, do noto change value in status table
            // -2 means special parameter deliberately set to OFF

            for (int i=0; i<pc.ParameterNames.Count; i++)
            {
                if (pc.ParameterNames[i].ToUpper() == "CH1")
                {
                    int.TryParse(pc.ParameterValues[i], out ch1);
                }
                else if (pc.ParameterNames[i].ToUpper() == "CH1D")
                {
                    ch1D = -2;
                    if (pc.ParameterValues[i] != "OFF")
                    {
                        int.TryParse(pc.ParameterValues[i], out ch1D);
                    }
                }
                else if (pc.ParameterNames[i].ToUpper() == "CH2")
                {
                    int.TryParse(pc.ParameterValues[i], out ch2);
                }
            }

            UpdateController(pc.wallId, ch1, ch1D, ch2);
        }

        public static void RefreshTimer(object sender, ElapsedEventArgs e)
        {
            if (!RefreshEnabled) return;

            for (int c = 0; c < ControllerStatuses.Count(); c++)
            {
                ControllerStatuses.Status status = ControllerStatuses.GetFromIndex(c);
                UpdateController(status.WallId, status.Ch1, status.Ch1D, status.Ch2);

                Thread.Sleep(Configuration.LightingRefreshDelayBetweenControllersMilliseconds);
            }

            ((System.Timers.Timer)sender).Interval = Configuration.LightingRefreshIntervalSeconds * 1000;
        }

    }

    #endregion

}
