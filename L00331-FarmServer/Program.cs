﻿
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;

namespace FarmServer
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("user32.dll", EntryPoint = "GetSystemMenu")]
        extern static IntPtr GetSystemMenu(IntPtr hWnd, IntPtr bRevert);
        [DllImport("user32.dll", EntryPoint = "RemoveMenu")]
        extern static IntPtr RemoveMenu(IntPtr hMenu, uint uPosition, uint uFlags);

        private static TelnetServer telnetServer;
        private static bool debugInfo = false;
        private static DateTime damatexLastCommandTimeStamp = DateTime.Now;

        private const string ServerVersion ="L00331-Server-1.0.3";

        static void DisableCloseButton()
        {
            IntPtr windowHandle = GetConsoleWindow();
            IntPtr closeMenu = GetSystemMenu(windowHandle, IntPtr.Zero);
            uint SC_CLOSE = 0xF060;
            RemoveMenu(closeMenu, SC_CLOSE, 0x0);

            //Console.WriteLine(ServerVersion);
            //Console.WriteLine(windowHandle);
            //Console.WriteLine(closeMenu);
        }

        static void Main(string[] args)
        {
            Console.Title = ServerVersion;
            DisableCloseButton();
            Console.Title = ServerVersion + " ****** USE 'Q' TO CLOSE CONSOLE ******";
            Console.SetWindowSize(80, 40);

            Console.WriteLine(ServerVersion);

            if (!Configuration.Load())
            {
                Console.WriteLine("Cannot load configuration, closing server...");
                Console.ReadLine();
                return;
            }

            if (!ControllerLink.CheckPort(Configuration.LightingRs485ComPort))
            {
                string errorMessage = "Cannot initialize COM port, closing server...";
                Log.Add(Configuration.ErrorLogFile, errorMessage);
                Console.WriteLine(errorMessage);
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Starting lighting service...");
            if (!LightingService.Initialize())
            {
                string errorMessage = "Cannot initialize lighting service, closing server...";
                Log.Add(Configuration.ErrorLogFile, errorMessage);
                Console.WriteLine(errorMessage);
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Starting telnet server...");
            TelnetServer();
        }

        private static void CheckDamatexTimeout()
        {
            if (Configuration.DamatexWatchdogTimeoutSeconds == 0) return;

            if (DateTime.Now.Subtract(damatexLastCommandTimeStamp).TotalSeconds >= Configuration.DamatexWatchdogTimeoutSeconds)
            {
                damatexLastCommandTimeStamp = DateTime.Now;

                Console.WriteLine("Damatex timeout - shutting down lighting service!");

                for (int room=Configuration.FirstRoom; room<=Configuration.LastRoom; room++)
                {
                    int roomBroadcastAddress = (room - 1) * Configuration.RoomOffset + Configuration.FirstWallOffset;
                    string command = string.Format("/{0}/CH1={1},CH2={1}", roomBroadcastAddress.ToString(), Configuration.DamatexWatchdogTimeoutPwmDutyCycle);
                    LightingService.HandleCommand(CommandParser.Parse(command));
                }

            }
        }

        private static void TelnetServer()
        {
            telnetServer = new TelnetServer(IPAddress.Any, Configuration.TelnetPort);
            telnetServer.ClientConnected += TelnetClientConnected;
            telnetServer.ClientDisconnected += TelnetClientDisconnected;
            telnetServer.MessageReceived += TelnetCommandReceived;
            telnetServer.Start();

            Console.WriteLine("Waiting for connections...");
            Console.WriteLine("Type q to quit");
            Console.WriteLine("     c to dump configuration to console");
            Console.WriteLine("     d to toggle debug verbose mode (off by default)");
            Console.WriteLine("     r to toggle cyclic refresh to A02320 (on by default)");
            Console.WriteLine("     s to dump current controller statuses on console (CH1,CH2,Dimming,etc).");
            Console.WriteLine("     t to list active telnet connections");
            Console.WriteLine("     x tp clear console");

            char c = ' ';
            while (c != 'q')
            {
                System.Threading.Thread.Sleep(500);
                CheckDamatexTimeout();

                if (Console.KeyAvailable)
                {
                    c = Console.ReadKey(false).KeyChar;
                    if (c == 'x')
                    {
                        Console.Clear();
                    }
                    else if (c == 'd')
                    {
                        debugInfo = !debugInfo;
                        Console.WriteLine("Debug verbose mode: " + (debugInfo ? "ON" : "OFF"));
                    }
                    else if (c == 'r')
                    {
                        LightingService.RefreshEnabled = !LightingService.RefreshEnabled;
                        Console.WriteLine("Cyclic refresh to A02320: " + (LightingService.RefreshEnabled ? "ON" : "OFF"));
                    }
                    else if (c == 'c')
                    {
                        Console.WriteLine("Configuration:");
                        Console.WriteLine("  Damatex watchdog (sec) : {0}", Configuration.DamatexWatchdogTimeoutSeconds);
                        Console.WriteLine("  Damatex watchdog PWM (%) : {0}", Configuration.DamatexWatchdogTimeoutPwmDutyCycle);
                        Console.WriteLine("  First room : {0}", Configuration.FirstRoom);
                        Console.WriteLine("  Last room : {0}", Configuration.LastRoom);
                        Console.WriteLine("  Lighting initial refresh interval (sec) : {0}", Configuration.LightingInitialRefreshIntervalSeconds);
                        Console.WriteLine("  Lighting refresh delay (msec) : {0}", Configuration.LightingRefreshDelayBetweenControllersMilliseconds);
                        Console.WriteLine("  Lighting refresh interval (sec) : {0}", Configuration.LightingRefreshIntervalSeconds);
                        Console.WriteLine("  Lighting RS485 port : {0}", Configuration.LightingRs485ComPort);
                        Console.WriteLine("  Lighting RS485 rx timeout (msec) : {0}", Configuration.LightingRs485RxTimeoutMilliseconds);
                        Console.WriteLine("  Max age of status file (min) : {0}", Configuration.MaxAgeOfStatusFileMinutes);
                        Console.WriteLine("  Server id : {0}", Configuration.ServerId);
                        Console.WriteLine("  Server Version : {0}", Configuration.ServerVersion);
                        Console.WriteLine("  Telnet port : {0}", Configuration.TelnetPort);
                        Console.WriteLine("  Error log file: {0}", Configuration.ErrorLogFile);
                        Console.WriteLine("  Comm log file: {0}", Configuration.CommLogFile);
                    }
                    else if (c == 's')
                    {
                        Console.WriteLine("Controller statuses:");
                        for (int controller = 0; controller < LightingService.ControllerStatuses.Count(); controller++)
                        {
                            LightingService.ControllerStatuses.Status status = LightingService.ControllerStatuses.GetFromIndex(controller);
                            Console.WriteLine("  ID=" + status.WallId.ToString("000") + "," +
                                (status.Alive ? "Status=Good" : "Status=Fail") + "," +
                                "CH1=" + status.Ch1.ToString("000") + "," +
                                "CH2=" + status.Ch2.ToString("000") + "," +
                                (status.Ch1D >= 0 ? "Mode=Dimming" : "Mode=Normal") +
                                (status.Ch1D >= 0 ? ",CH1D=" + status.Ch1D.ToString("000") : ""));
                        }
                    }
                    else if (c == 't')
                    {
                        List<string> clientList = telnetServer.GetClientList();
                        Console.WriteLine("Telnet clients: {0}", clientList.Count);
                        foreach(string client in clientList)
                        {
                            Console.WriteLine(client);

                        }
                    }
                }
            }

            telnetServer.Stop();
        }

        private static void TelnetClientConnected(TelnetClient c)
        {
            Console.WriteLine("Connected: " + c);
        }

        private static void TelnetClientDisconnected(TelnetClient c)
        {
            Console.WriteLine("Disconnected: " + c);
        }

        private static void TelnetCommandReceived(TelnetClient c, string command)
        {
            if (debugInfo)
            {
                Console.WriteLine("Command: " + command);
            }

            CommandParser.ParsedCommand pc = CommandParser.Parse(command);

            if (!pc.Success)
            {
                if (debugInfo)
                {
                    Console.WriteLine("  Parsing error!");
                }

                telnetServer.SendMessageToClient(c, "ERROR\r\n");
            }
            else
            {
                damatexLastCommandTimeStamp = DateTime.Now;

                if (debugInfo)
                {
                    Console.WriteLine("  Raw : " + pc.RawCommand);
                    Console.WriteLine("  Controller : " + pc.wallId.ToString());
                    Console.WriteLine("  Handler : " + pc.Handler.ToString());
                    for(int i=0; i<pc.ParameterNames.Count; i++)
                    {
                        Console.WriteLine("  Name  : " + pc.ParameterNames[i]);
                        Console.WriteLine("  Value : " + pc.ParameterValues[i]);
                    }
                }

                switch (pc.Handler)
                {
                    case CommandParser.CommandHandler.Server:
                        if (debugInfo)
                        {
                            Console.WriteLine("  Processing internal command");
                        }
                        string sid = "/" + Configuration.ServerId.ToString("000") + "/";
                        switch(pc.ParameterNames[0])
                        {
                            case "VERSION":
                                telnetServer.SendMessageToClient(c, sid + "VERSION=" + Configuration.ServerVersion + "\r\n");
                                break;
                            case "WDOG":
                                telnetServer.SendMessageToClient(c, sid + "WDOG=" + LightingService.ControllerStatuses.SimpleFormat() + "\r\n");
                                break;
                            case "STAT":
                                telnetServer.SendMessageToClient(c, sid + "STAT=" + LightingService.ControllerStatuses.NormalFormat(pc.ParameterValues[0]) + "\r\n");
                                break;
                            case "STATH":
                                telnetServer.SendMessageToClient(c, sid + "STATH=\r\n" + LightingService.ControllerStatuses.HumanReadableFormat() + "\r\n");
                                break;
                        }
                        break;

                    case CommandParser.CommandHandler.LightingService:
                        if (debugInfo)
                        {
                            Console.WriteLine("  Processing lighting service command");
                        }
                        bool dimmingEngaged = LightingService.DimmingEngaged(pc);
                        telnetServer.SendMessageToClient(c, "OK" + (dimmingEngaged ? ",DIMMING" :"") + "\r\n");
                        LightingService.HandleCommand(pc);
                        break;
                }
            }
        }

    }

}

