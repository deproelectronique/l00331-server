﻿
using System.Collections.Generic;

namespace FarmServer
{
    public static class CommandParser
    {
        public enum CommandHandler
        {
            LightingService = 1,
            Broadcast,
            Server,
        }

        public class ParsedCommand
        {
            public bool Success;
            public string RawCommand;
            public int wallId;
            public List<string> ParameterNames;
            public List<string> ParameterValues;
            public CommandHandler Handler;
            public ParsedCommand() { }
        }

        public static bool AcceptableWallAddress(int wallId)
        {
            if (wallId == Configuration.ServerId) return true;

            for (int room = Configuration.FirstRoom; room <= Configuration.LastRoom; room++)
            {
                int roomBaseAddressSouthWall = (room - 1) * Configuration.RoomOffset + Configuration.FirstWallOffset;
                if (wallId >= roomBaseAddressSouthWall && wallId <= roomBaseAddressSouthWall + Configuration.NbSouthWalls) return true;

                int roomBaseAddressNorthWall = (room - 1) * Configuration.RoomOffset + Configuration.FirstWallOffset + Configuration.NorthWallOffset;
                if (wallId >= roomBaseAddressNorthWall && wallId <= roomBaseAddressNorthWall + Configuration.NbNorthWalls) return true;
            }

            return false;
        }

        public static ParsedCommand Parse(string command)
        {
            ParsedCommand pc = new ParsedCommand {
                Success = false,
                RawCommand = command,
                wallId = -1,
                Handler = CommandHandler.Server,
            };
            pc.ParameterNames = new List<string>();
            pc.ParameterValues = new List<string>();

            try
            {
                // Protocol: /<wall id>/<parameter>[=<value>][,<parameter>[=<value>]]
                // Example:  /106/CH1=99,CH2=98
                // No CRC expected from Damatex

                command = command.ToUpper();
                command = command.Replace("\r", "");
                command = command.Replace("\n", "");

                if (!command.StartsWith("/")) return pc;

                // Wall Id

                command = " " + command.Substring(1);
                int x1 = command.IndexOf('/');
                if (x1 == -1) return pc;
                int.TryParse(command.Substring(1, x1 - 1), out pc.wallId);

                if (!AcceptableWallAddress(pc.wallId)) return pc;

                // Parameters

                command = command.Substring(x1 + 1);
                string[] pairs = command.Split(',');
                if (pairs.Length == 0) return pc;

                string value = "";
                int i = 0;

                foreach(string pair in pairs)
                {
                    string[] split = pair.Split('=');

                    pc.ParameterNames.Add(split[0]);
                    value = (split.Length > 1 ? split[1] : "");
                    pc.ParameterValues.Add(value);

                    // Not the ideal place to validate, should bein the switch() below to adapt to different commands

                    // Associate a pair (parameter=value) with a service
                    // Not handled: Damatex sends a command with parameters aimed at different services

                    switch (split[0])
                    {
                        case "CH1":
                        case "CH1D":
                        case "CH2":
                            if (pc.wallId != Configuration.ServerId)
                            {
                                if (!(split[0] == "CH1D" && value == "OFF"))
                                {
                                    int.TryParse(value, out i);
                                    if (i < 0 || i > 100) return pc;
                                }
                                pc.Handler = CommandHandler.LightingService;
                                pc.Success = true;
                            }
                            break;

                        case "VERSION":
                        case "WDOG":
                        case "STAT":
                        case "STATH":
                            if (pc.wallId == Configuration.ServerId)
                            {
                                pc.Handler = CommandHandler.Server;
                                pc.Success = true;
                            }
                            break;

                        default:
                            break;
                    }
                }

            }
            catch {}

            return pc;
        }

    }

}

