﻿
using System;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace FarmServer
{
    static class ControllerLink
    {
        private static Mutex mutex = new Mutex();

        private static class SimpleSerialPort
        {
            public static SerialPort SerialPort { get; private set; }

            const int defaultBaudRate = 9600;

            public static bool Open(string comPort, int baudRateValue = defaultBaudRate)
            {
                try
                {
                    SerialPort = new System.IO.Ports.SerialPort(comPort, baudRateValue);

                    SerialPort.BaudRate = baudRateValue;
                    SerialPort.DtrEnable = false;
                    SerialPort.RtsEnable = false;
                    SerialPort.Parity = Parity.None;
                    SerialPort.DataBits = 8;
                    SerialPort.StopBits = StopBits.One;
                    SerialPort.Handshake = Handshake.None;
                    SerialPort.Open();

                    return true;
                }
                catch (Exception ex)
                {
                    Log.Add(Configuration.ErrorLogFile, "Cannot open COM port:" + ex.Message);
                    return false;
                }
            }

            public static bool Close()
            {
                try
                {
                    SerialPort.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    Log.Add(Configuration.ErrorLogFile, "Cannot close COM port:" + ex.Message);
                    return false;
                }
            }

            public static string ReadLine()
            {
                try
                {
                    // Will block until line is read or readTimeOut exceeded
                    return SerialPort.ReadLine();
                }
                catch
                {
                    return "";
                }
            }

            public static bool Flush()
            {
                try
                {
                    SerialPort.BaseStream.Flush();
                    return true;
                }
                catch (Exception ex)
                {
                    Log.Add(Configuration.ErrorLogFile, "Cannot flush COM port:" + ex.Message);
                    return false;
                }
            }

            public static bool WriteLine(string text)
            {
                try
                {
                    SerialPort.Write(text + "\r\n");
                    return true;
                }
                catch
                {
                    // SPIGEON this would flood the log !Log.AddToLog("Cannot write COM port:" + ex.Message);
                    return false;
                }
            }

        }

        private static string GenerateCrc(string data)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(data);
            const ushort poly = 4129;
            ushort[] table = new ushort[256];
            ushort initialValue = 0xffff;   // Initial value for CCITT 16-bit - use 0x0000 for xmodem
            ushort temp, a;
            ushort crc = initialValue;

            for (int i = 0; i < table.Length; ++i)
            {
                temp = 0;
                a = (ushort)(i << 8);
                for (int j = 0; j < 8; ++j)
                {
                    if (((temp ^ a) & 0x8000) != 0)
                        temp = (ushort)((temp << 1) ^ poly);
                    else
                        temp <<= 1;
                    a <<= 1;
                }
                table[i] = temp;
            }

            for (int i = 0; i < bytes.Length; ++i)
            {
                crc = (ushort)((crc << 8) ^ table[((crc >> 8) ^ (0xff & bytes[i]))]);
            }

            return crc.ToString("X4");
        }

        public static bool CheckPort(string comPort)
        {
            if (SimpleSerialPort.Open(comPort))
            {
                SimpleSerialPort.Close();
                return true;
            }
            return false;
        }

        public static bool Relay(string comPort, int readTimeout, string command)
        {
            // We don't hanbdle priorities or queues, just contention
            // It is assumed that controllers respond within a few milliseconds, so the physical 
            // port will never be blocked longer than that - unless there is a timeout (also short)

            mutex.WaitOne();

            string response = "";
            command = command.ToUpper();

            if (SimpleSerialPort.Open(comPort))
            {
                SimpleSerialPort.SerialPort.ReadTimeout = readTimeout;
                SimpleSerialPort.Flush();

                command = command + "," + GenerateCrc(command);
                SimpleSerialPort.WriteLine(command);

                string verbose = ">>> RS-485: " + command;
                Console.WriteLine(verbose);
                Log.Add(Configuration.CommLogFile, verbose);

                response = SimpleSerialPort.ReadLine().ToUpper();
                response = response.Replace("\r", "");
                response = response.Replace("\n", "");
                SimpleSerialPort.Close();

                verbose = "<<< RS-485: " + response;
                Console.WriteLine(verbose);
                Log.Add(Configuration.CommLogFile, verbose);

            }

            if (response != "")
            {
                string[] split = response.Split(',');
                if (split.Length <= 1 || response.ToUpper().Contains("/ERROR"))
                {
                    response = "";
                }
                else
                {
                    string crc = split[split.Length - 1];
                    response = response.Replace("," + crc, "");
                    string expectedCrc = GenerateCrc(response);
                    if (crc != expectedCrc)
                    {
                        response = "";
                    }
                }
            }

            mutex.ReleaseMutex();

            return response != "";
        }
    }

}

