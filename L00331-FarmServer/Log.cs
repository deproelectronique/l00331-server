﻿
using System;
using System.IO;
using System.Threading;

namespace FarmServer
{
    static class Log
    {
        private static Mutex mutex = new Mutex();

        private static readonly string defaultLogFile = "FarmServerLog.txt";

        private static string TimeStamp()
        {
            DateTime dt = DateTime.Now;
            return dt.Year.ToString("0000") + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00") + " " +
                    dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") + ":" + dt.Second.ToString("00") + "." + dt.Millisecond.ToString("000");
                    
        }

        public static void Add(string logFile, string logEvent)
        {
            mutex.WaitOne();

            if (string.IsNullOrEmpty(logFile)) logFile = defaultLogFile;

            if (!File.Exists(logFile))
            {
                using (StreamWriter sw = File.CreateText(logFile))
                {
                    sw.WriteLine(TimeStamp() + " New Log File");
                }
            }

            // Maybe check if file has become too big?

            using (StreamWriter sw = File.AppendText(logFile))
            {
                sw.WriteLine(TimeStamp() + " " + logEvent);
            }

            mutex.ReleaseMutex();
        }

    }

}
