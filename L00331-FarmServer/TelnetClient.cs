﻿
// Thanks are owed to examples found on the web :-)

using System;
using System.Net;

namespace FarmServer
{
    public class TelnetClient
    {
        private uint id;
        private IPEndPoint remoteAddr;
        private DateTime connectedAt;
        private string receivedData;

        public TelnetClient(uint clientId, IPEndPoint remoteAddress)
        {
            this.id = clientId;
            this.remoteAddr = remoteAddress;
            this.connectedAt = DateTime.Now;
            this.receivedData = string.Empty;
        }

        public uint GetClientID()
        {
            return id;
        }

        public string GetReceivedData()
        {
            return receivedData;
        }

        public void AppendReceivedData(string dataToAppend)
        {
            this.receivedData += dataToAppend;
        }

        public void RemoveLastCharacterReceived()
        {
            receivedData = receivedData.Substring(0, receivedData.Length - 1);
        }

        public void ResetReceivedData()
        {
            receivedData = string.Empty;
        }

        public override string ToString()
        {
            string ip = string.Format("{0}:{1}", remoteAddr.Address.ToString(), remoteAddr.Port);
            string res = string.Format("Client #{0} (From: {1}, Connection time: {2})", id, ip, connectedAt);
            return res;
        }
    }
}
